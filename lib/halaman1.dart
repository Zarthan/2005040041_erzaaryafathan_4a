import 'package:flutter/material.dart';
import 'homepage.dart';

class Halaman1 extends StatelessWidget {
  final String imgUrl;
  Halaman1(this.imgUrl);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Vladimir Lenin'),
      ),
      backgroundColor: Color.fromRGBO(20, 20, 20, 1),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            child: Image.network(imgUrl),
          ),
          ElevatedButton(
            onPressed: (){
              Navigator.pop(
                context, MaterialPageRoute(builder: (context) => Homepage())
              );
            }, 
            child: Text("Back")
          )
        ],
      ),
    );
  }
}

//"https://cdn.britannica.com/13/59613-050-D57A3D88/Vladimir-Ilich-Lenin-1918.jpg"