import 'package:flutter/material.dart';
import 'homepage.dart';

class Halaman2 extends StatelessWidget {
  final String imgUrl;
  Halaman2(this.imgUrl);
  @override

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Joseph Stalin'),
      ),
      backgroundColor: Color.fromRGBO(20, 20, 20, 1),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            child: Image.network(imgUrl),
          ),
          ElevatedButton(
            onPressed: (){
              Navigator.pop(
                context, MaterialPageRoute(builder: (context) => Homepage())
              );
            }, 
            child: Text("Back")
          )
        ],
      ),
    );
  }
}

//"https://cdn.britannica.com/33/229033-050-73CE4FAB/Joseph-Stalin.jpg"