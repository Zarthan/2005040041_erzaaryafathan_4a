import 'package:flutter/material.dart';
import 'homepage.dart';

class Halaman3 extends StatelessWidget {
  final String imgUrl;
  Halaman3(this.imgUrl);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mikhail Gorbachev'),
      ),
      backgroundColor: Color.fromRGBO(20, 20, 20, 1),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            child: Image.network(imgUrl),
          ),
          ElevatedButton(
            onPressed: (){
              Navigator.pop(
                context, MaterialPageRoute(builder: (context) => Homepage())
              );
            }, 
            child: Text("Back")
          )
        ],
      ),
    );
  }
}


//"https://cdn.britannica.com/85/177585-050-5BE02E0D/Mikhail-Gorbachev-1987.jpg"