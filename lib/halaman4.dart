import 'package:flutter/material.dart';
import 'homepage.dart';

class Halaman4 extends StatelessWidget {
  final String imgUrl;
  Halaman4(this.imgUrl);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nikita Khrushchev'),
      ),
      backgroundColor: Color.fromRGBO(20, 20, 20, 1),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            child: Image.network(imgUrl),
          ),
          ElevatedButton(
            onPressed: (){
              Navigator.pop(
                context, MaterialPageRoute(builder: (context) => Homepage())
              );
            }, 
            child: Text("Back")
          )
        ],
      ),
    );
  }
}
