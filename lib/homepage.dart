import 'package:flutter/material.dart';
import 'package:flutter_application_3/halaman1.dart';
import 'package:flutter_application_3/halaman4.dart';

import 'halaman2.dart';
import 'halaman3.dart';

class Homepage extends StatelessWidget {
  //const Homepage({super.key});
  const Homepage({Key? key}) : super(key: key);
  static const List<String> imgAddress = [
    "https://cdn.britannica.com/13/59613-050-D57A3D88/Vladimir-Ilich-Lenin-1918.jpg",
    "https://cdn.britannica.com/33/229033-050-73CE4FAB/Joseph-Stalin.jpg",
    "https://cdn.britannica.com/85/177585-050-5BE02E0D/Mikhail-Gorbachev-1987.jpg",
    "https://cdn.britannica.com/86/10486-004-44B11963/Nikita-Khrushchev-1960.jpg"
  ];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(scaffoldBackgroundColor: Color.fromARGB(255, 20, 20, 20)),
      home: Scaffold(
          appBar: AppBar(title: Text(style: TextStyle(fontSize: 30)," Project")),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        height: 100,
                        width: 100,
                        child: Image.network(imgAddress[0]),
                      ),  
                      ElevatedButton(
                        onPressed: (){
                          Navigator.push(
                            context, MaterialPageRoute(builder: (context) => Halaman1(imgAddress[0])));
                        }, child: Text("Lihat"))
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        height: 100,
                        width: 100,
                        child: Image.network(imgAddress[1]),
                      ),  
                      ElevatedButton(
                        onPressed: (){
                          Navigator.push(
                            context, MaterialPageRoute(builder: (context) => Halaman2(imgAddress[1])));
                        }, child: Text("Lihat"))
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        height: 100,
                        width: 100,
                        child: Image.network(imgAddress[2]),
                      ),  
                      ElevatedButton(
                        onPressed: (){
                          Navigator.push(
                            context, MaterialPageRoute(builder: (context) => Halaman3(imgAddress[2])));
                        }, child: Text("Lihat"))
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        height: 100,
                        width: 100,
                        child: Image.network(imgAddress[3]),
                      ),  
                      ElevatedButton(
                        onPressed: (){
                          Navigator.push(
                            context, MaterialPageRoute(builder: (context) => Halaman4(imgAddress[3])));
                        }, child: Text("Lihat"))
                    ],
                  ),
                ],
              ),
            ],
          )),
    );
  }
}